Source: assetfinder
Section: golang
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any
Testsuite: autopkgtest-pkg-go
Standards-Version: 4.6.2
Homepage: https://github.com/tomnomnom/assetfinder
Vcs-Browser: https://salsa.debian.org/pkg-security-team/assetfinder
Vcs-Git: https://salsa.debian.org/pkg-security-team/assetfinder.git
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/tomnomnom/assetfinder

Package: assetfinder
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: Find domains and subdomains related to a given domain
 assetfinder is a command-line tool designed to find domains and
 subdomains associated with a specific domain.
 .
 The main objective of the tool is to help security researchers and IT
 professionals discover and understand how the domains and sub-domains
 of a given organization are distributed, trying to find possible
 security flaws and vulnerabilities.
 .
 assetfinder uses multiple data sources to perform its research, including:
  - crt.sh
  - certspotter
  - hackertarget
  - threatcrowd
  - Wayback Machine
  - dns.bufferover.run
  - Facebook Graph API
  - Virustotal
  - findsubdomains
 This expands coverage and increases the accuracy of results.
